<?php

namespace Drupal\simple_sharing_links\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a block with sharing links.
 *
 * @Block(
 *   id = "simple_sharing_links",
 *   admin_label = @Translation("Simple sharing links")
 * )
 */
class SharingLinksBlock extends BlockBase {
  /**
    * {@inheritdoc}
    */
  public function build() {
    // $links = [
    //   '#theme' => 'links',
    //   '#attributes' => [
    //     'class' => ['simple_sharing_links'],
    //   ],
    //   '#links' => [
    //     ['title' => '<span class="andale">titulo</span>', 'url' => Url::fromUri('http://www.facebook.com'), 'html' => TRUE],
    //   ]
    // ];

    $links = $this->getLinks();
    $links_render = [
      '#theme' => 'links',
      '#attributes' => [
        'class' => 'sharing_links',
      ],
      '#links' => $links,
    ];

    $build = [
      '#cache' => ['contexts' => ['url.path']],
      'links_wrapper' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'sharing_links__wrapper',
        ],
        'icon' => [
          '#type' => 'markup',
          '#markup' => '<div class="sharing_links__button"><span class="sharing_links__button_text">' . $this->t('Share') . '</span></div>',
        ],
        'links' => $links_render,
      ]
    ];

    return $build;
  }

  /**
    * Produces the array of links to be presented in the sharing widget
    */
  private function getLinks() {
    // TODO: use plugins to offer more link targets and options (ie Pinterest share with JS)
    // TODO: add a block configuration form to allow users to choose the targets

    $current_url = $this->getCurrentUrl();
    $encoded_url = urlencode($current_url);

    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    $page_title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());

  
    // TODO: understand how the getTitle method, called above, works, and fix this clunky mess:

    $config = \Drupal::config('system.site');
    if(!is_string($page_title)) {
      $title_class = (!is_array($page_title)) ? get_class($page_title) : FALSE;

      // For some reason I don't understand, the page title sometimes comes as an Array,
      // for example when visiting a user profile. In these cases, we default to the
      // site name
      if($title_class == 'Drupal\Core\StringTranslation\TranslatableMarkup') {
        $encoded_title = urlencode($config->get('name') . ' ' . $page_title->render());
      }
      else {
        $encoded_title = urlencode($config->get('name'));
      }
    }
    else {
      $encoded_title = urlencode($config->get('name') . ' ' . $page_title);
    }

    $links = [];
    
    // Facebook
    $fb_share = 'https://www.facebook.com/sharer/sharer.php?u='; 
    $links[] = [
      'title' => $this->t('Facebook'),
      'url' => Url::fromUri($fb_share . $encoded_url),
      'attributes' => ['class' => ['sharing_link--facebook']],
    ];

    // Twitter
    $tw_share = 'https://twitter.com/intent/tweet?text=';
    $links[] = [
      'title' => $this->t('Twitter'),
      'url' => Url::fromUri($tw_share . $encoded_title . ' ' . $encoded_url),
      'attributes' => ['class' => ['sharing_link--twitter']],
    ];

    foreach ($links as &$link) {
      if(!isset($link['attributes'])) {
        $link['attributes'] = [];
      }
      if(!isset($link['attributes']['class'])) {
        $link['attributes']['class'] = [];
      }

      $link['attributes']['target'] = '_blank';
      $link['attributes']['class'][] = 'sharing_link';
    }

    return $links;
  }
  
  /**
    * Get the URL of the current page
    */
  private function getCurrentUrl() {
    $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return $current_url;
  }
}
